# SPECIFICATION
  NAME <- "EXPLORE_VARIABLES.R"
  # FUNCTIONS: 
  #   Explore Individual Variables 
  # AUTHOR:xyw
  # NOTE
  #   1. This R script is tested in R.3.0.1 on WIN32 platform.
  #   2. ...
  # LOG
  #   VERSION: 2013.11.20
  print(paste("Loading ",NAME,"..."))
  rm(NAME)
EXPLORE_VARIABLES <- function(list) {
  summary(list)
  quantile(list)
  quantile(list,c(.1, .2, .5, .8, 1))
  var(list)
  
  plot(density(list))
  table(list)
  pie(table(list))
  barplot(table(list))
}
