# SPECIFICATION
  NAME <- "CV_KNNreg.R"
  #     
  # FUNCTIONS:
  #    1. cross validation and knn regression
  #    2. This script is required by acute_tox_R_O_pre_knn.R
  #      
  # AUTHOR:xyw
  # NOTE
  #   1. This R script is tested in R.3.0.2 on WIN32 platform.
  #   2. required packages:
  #       1) "caret" 
  #         
  #         
  #         
# LOG
  VERSION <- "2014.01.22"
  #   created.
  #


# CODES
  
  print(paste("Loading ",NAME,". Version ", VERSION))
  rm(NAME, VERSION)
  
  # @ sdfset,  SDF data set
cv_knnreg <- function(sdfset) {

  
  
  